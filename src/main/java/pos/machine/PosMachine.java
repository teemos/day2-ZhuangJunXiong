package pos.machine;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class PosMachine {
    public String printReceipt(List<String> barcodes) {

        List<Goods>goods = getGoods(barcodes);
        int totalPrice = getTotalPrice(goods);
        String result = printAll(goods,totalPrice);
        return result;
    }

    public boolean isVaild(String barcode){
        List<Item> items = ItemsLoader.loadAllItems();
        for (Item item : items) {
            if (item.getBarcode().equals(barcode)) {
                return true;
            }
        }
        return false;
    }

    public List<String> getDistinctList (List<String> barcode){
        List<String> distinctList = barcode.stream().distinct().collect(Collectors.toList());
        return distinctList;
    }

    public int getItemCount(String barcode, List<String> barcodes){
        int count = 0;
        for (String bar : barcodes) {
            if (barcode == bar){
                count++;
            }
        }
        return count;
    }

    public int getItemPrice(String barcode, List<String> barcodes){
        int itemPrice = 0;
        int count = getItemCount(barcode, barcodes);
        List<Item> items = ItemsLoader.loadAllItems();
        for (Item item : items) {
            if (item.getBarcode().equals(barcode)) {
                int price = item.getPrice();
                itemPrice = price * count;
            }
        }
        return itemPrice;
    }

    public List<Goods> getGoods(List<String> barcode){
        List<String> distinctList = getDistinctList(barcode);
        List<Goods> goods = new ArrayList<>();
        for (String bar : distinctList) {
            if(!isVaild(bar)){
                break;
            }
            int count = getItemCount(bar,barcode);
            int price = getItemPrice(bar,barcode);
            Goods goods1 = new Goods(bar, price, count);
            goods.add(goods1);
        }
        return goods;
    }
    public int getTotalPrice(List<Goods>goods){
        int totalPrice = 0;
        for (Goods goods1 : goods) {
            totalPrice += goods1.getItemPrice();
        }
        return totalPrice;
    }

    public String printHead(){
        return "***<store earning no money>Receipt***";
    }
    public String printHr(){
        return "----------------------";
    }
    public String printFoot(){
        return "**********************";
    }
    public String printTotalPrice(int totalPrice){
        return "Total: " + totalPrice + " (yuan)";
    }
    public String printGoods(List<Goods> goods){
        String result = "";
        //Name: Coca-Cola, Quantity: 4, Unit price: 3 (yuan), Subtotal: 12 (yuan)
        for (int i=0; i< goods.size();i++){
            int price = 0;
            String name = "";
            Goods goods1 = goods.get(i);
            List<Item> items = ItemsLoader.loadAllItems();
            for (Item item : items) {
                if (item.getBarcode().equals(goods1.getName())) {
                    price = item.getPrice();
                    name = item.getName();
                }
            }
            if (i!=0){
                result+="\n";
            }
            result += "Name: " +name +", Quantity: " +goods1.getItemCount();
            result += ", Unit price: " +price +" (yuan), Subtotal: " +goods1.getItemPrice()+" (yuan)";
        }
        return result;
    }
    public String printAll(List<Goods> goods, int totalPrice){
        String result = "";
        result += printHead()+"\n";
        result += printGoods(goods)+"\n";
        result += printHr()+"\n";
        result += printTotalPrice(totalPrice)+"\n";
        result += printFoot();
        return result;
    }

}
