package pos.machine;

public class Goods {
    private final String name;
    private final int itemPrice;
    private final int itemCount;

    public Goods(String name, int itemPrice, int itemCount) {
        this.name = name;
        this.itemPrice = itemPrice;
        this.itemCount = itemCount;
    }

    public String getName() {
        return name;
    }

    public int getItemPrice() {
        return itemPrice;
    }

    public int getItemCount() {
        return itemCount;
    }
}
