# O
Today we spent the whole day only delving into task decomposition. Apart from learning concepts, we spent most of our time practicing on our own. First, we drew a task decomposition diagram, and then wrote code based on it. In addition, we also briefly reviewed the knowledge of git and applied it to coding.

# R
Nice

# I
The method of task decomposition is particularly excellent, as it allows us to fully understand the requirements of the project and makes coding easier

# D
When writing code in the future, I will use task decomposition to.
